package asgn1Collection;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;

public class MovieListing implements Listing {

	private Set<String> keywords = new HashSet<String>();
	private String title;
	private int year;
	private BitSet keyVectors = new BitSet();

	public MovieListing(String trimTitle, int parseYear) {
		title = trimTitle;
		year = parseYear;
	}

	public void addKeyword(String kw) throws ListingException {
		if (kw != null) {
			if (kw != "") {
				keywords.add(kw);
			} else {
				throw new ListingException("Keyword cannot be empty.");
			}
		} else {
			throw new ListingException("Keyword does not exist.");
		}
	}

	public String getTitle() {
		return title;
	}

	@Override
	public int findSimilarity(Listing l) throws ListingException {
		int similarities = 0;
		for (int i = 0; i < this.keyVectors.size(); i++) {
			if (this.keyVectors == null) {
				throw new ListingException("KeyVector is null.");
			} else if (l == null) {
				throw new ListingException("Listing is null.");
			} else if (this.keyVectors == l) {
				similarities++;
			}
		}
		return similarities;
	}

	@Override
	public BitSet getKeyVector() throws ListingException {
		if (this.keyVectors != null) {
			return keyVectors;
		} else {
			throw new ListingException("KeyVector is Null.");
		}
	}

	@Override
	public Set<String> getKeywords() {
		return this.keywords;
	}

	@Override
	public int getYear() {
		return year;
	}

	@Override
	public int numKeywords() {
		return this.keywords.size();
	}

	@Override
	public void setKeyVector(BitSet bs) {
		keyVectors = bs;
	}

	@Override
	public String writeKeyVector() throws ListingException {
		if (this.keyVectors != null) {
			return this.keyVectors.toString();
		} else {
			throw new ListingException("KeyVector is Null.");
		}
	}

	@Override
	public String writeKeywords() {
		String str = "";
		int index = 0;
		for (String kw : this.getKeywords()) {
			str += kw + ":";
			index++;
			if ((index % 10) == 0) {
				str += "\n";
			}
		}
		return str;
	}

	@Override
	public String toString() {
		return this.getTitle() + ":" + this.getYear() + ":"
				+ "Active keywords:" + this.numKeywords();
	}

}
